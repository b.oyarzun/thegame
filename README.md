# Contenido

[[_TOC_]]

El directorio tiene la siguiente estructura:

# Estructura del proyectos

## src

### dashboard

Directorio que contiene el proyecto del dashboard o panel de control del rover.

Las tecnologias utilizadas fueron las siguientes:

- [React.js](https://reactjs.org/)
- [NodeJS](https://nodejs.org/en/)
- [Template Vuexy](https://themeforest.net/item/vuexy-vuejs-html-laravel-admin-dashboard-template/23328599)
- [Docker](https://www.docker.com/)

Debido a que el proyecto utiliza Docker, no es necesario preocuparse de las versiones de cada una de estas tecnologias, ya que para configurar el entorno de desarrollo, solo basta con ejecutar una instrucción en la linea de comandos.

`docker-compose up`

### modelo_3d

Directorio que contiene los archivos de corte del frame del rover.

### raspberry_pi

Contiene todos los artefactos necesarios para entregar la funcionalidad correspondiente al rover. 

#### ros2_foxy

Directorio que contiene el proyecto ROS2 Foxy el cual controla todos los periféricos conectados a la Raspberry.

- Encoder.
- Lidar.
- Ultrasonicos.
- Cámara.


Cada uno de los perifericos que reciben o transmiten datos esta representado como un nodo ROS. Es necesario comprender el funcionamiento de ROS2 Foxy para realizar modificaciones al código. Debido a que ROS tiene una terminologia extensa, puede puede volver muy compleja la tarea de leer código sin antes comprender su funcionamiento base.

Solicite acceso al curso de ROS2 Foxy que se encuentra en nuestra cuenta de Udemy.

#### sensors

Directorio que contiene scripts desarrollados en Python 3 para recibir señal de los sensores y manejar sus datos.
Los script almacenados en este directorio son utilizados en el proyecto _ros2_foxy_. Básicamente, aquí se desarrollaron los módulos Python 3 para realizar una fácil integración con los nodos ROS2.

#### Configuración del entorno de desarrollo en Raspberry PI

- Instalar Ubuntu 20.04 LTS en Raspberry PI utilizando [Raspberry Pi Imager](https://www.raspberrypi.com/software/)
- Copiar el directorio raspberry_pi en la ruta /home/ubuntu de la Raspberry PI.
- Ejecutar el archivo 1.install_raspberry_environment.bash el cual configurará completamente la Raspberry PI para su funcionamiento deseado. Para más información, lea los comentarios que se encuentran dentro del archivo 1.install_raspberry_environment.bash.

# ROS2
## Comandos de utilidad

`colcon build`

`source install/setup.bash`

`ros2 run ultrasonic_pkg ultrasonic_node`

`ros2 topic list`

`ros2 topic echo /<topic_name>`

`ros2 topic hz /<topic_name>`

`ros2 topic bw /<topic_name>`

`ros2 topic pub -r <hz> /<topic_name> <data_type> <data>`

`ros2 topic pub -r 1 -n ultrasonic_1 /ultrasonic_1 std_msgs/msg/Float32 "{data: 37.0}" &`

`ros2 topic pub -r 1 /ultrasonic_1 std_msgs/msg/Float32 "{data: 37.0}" &`

## ros websocket server (e.g. ws://0.0.0.0:9090) (raspberry)

`git clone https://github.com/RobotWebTools/rosbridge_suite.git`

`cd rosbridge_server`

`colcon build`

`source install/setup.bash`

`sudo apt install ros-foxy-rosbridge-server`

`ros2 launch rosbridge_server rosbridge_websocket_launch.xml`

## Ultrasonic data simulation

```
ros2 topic pub -r 1 -n ultrasonic_1 /ultrasonic_1 std_msgs/msg/Float32 "{data: 37.0}" & \
ros2 topic pub -r 1 -n ultrasonic_2 /ultrasonic_2 std_msgs/msg/Float32 "{data: 37.0}" & \
ros2 topic pub -r 1 -n ultrasonic_3 /ultrasonic_3 std_msgs/msg/Float32 "{data: 37.0}" & \
ros2 topic pub -r 1 -n ultrasonic_4 /ultrasonic_4 std_msgs/msg/Float32 "{data: 37.0}" & \
ros2 topic pub -r 1 -n ultrasonic_5 /ultrasonic_5 std_msgs/msg/Float32 "{data: 37.0}" & \
ros2 topic pub -r 1 -n ultrasonic_6 /ultrasonic_6 std_msgs/msg/Float32 "{data: 37.0}" & \
```

## Encoder data simulation

```
ros2 topic pub -r 1 -n encoder_1 /encoder_1 std_msgs/msg/Float32 "{data: 37.0}" & \
ros2 topic pub -r 1 -n encoder_2 /encoder_2 std_msgs/msg/Float32 "{position: 38.0}" & \
ros2 topic pub -r 1 -n encoder_3 /encoder_3 std_msgs/msg/Float32 "{position: 39.0}" & \
ros2 topic pub -r 1 -n encoder_4 /encoder_4 std_msgs/msg/Float32 "{position: 40.0}" & \
```

## Creating custom ros2 interfaces

Documentation: https://docs.ros.org/en/foxy/Tutorials/Beginner-Client-Libraries/Custom-ROS2-Interfaces.html

`ros2 topic pub -r 1 -n encoder_1 /encoder_1 rover_interfaces/msg/Encoder "{position: 37.0}"`

`ros2 interface show rover_interfaces/msg/Encoder`


# SITL

- [STIL ArduPilot](https://ardupilot.org/dev/docs/sitl-simulator-software-in-the-loop.html#sitl-simulator-software-in-the-loop)


# Simulation with Docker

Requerimientos

- [git](https://git-scm.com/downloads)
- [docker](https://www.docker.com/products/docker-desktop/)
- [QGroundControl](https://docs.qgroundcontrol.com/master/en/getting_started/download_and_install.html)

Es posible simular un rover en caso que no tenga acceso a la versión fisica. Gracias a la utilización de Docker, el proceso de instalación es sencillo. Para configurar el entorno de simulación, siga los siguientes pasos:

- Clonar el repositorio de ArduPilot

`git clone https://github.com/your-github-userid/ardupilot`

`cd ardupilot`

`git submodule update --init --recursive`

- Crear un container con la imagen de ArduPilot

`docker build . -t ardupilot`

- Dentro del directorio ardupilot ejecutar el comando:

`docker run --rm --privileged --network host -it -v $PWD:/ardupilot ardupilot:latest bash`

- Dentro del contenedor ejecutar los comandos:

`export PATH=$PATH:$HOME/.local/bin`

`sim_vehicle.py -v Rover`

En este punto, usted ha creado un entorno de simulación con un vehiculo rover, el cual esta disponible para su utización en los puertos 14550 y 14551 los cuales son generalmente utilizados por la estación de control en tierra (QGroundControl) y su script de control (Python) respectivamente. 
<!-- sim_vehicle.py --no-mavproxy --mcast -v Rover -->

`mavproxy.py --master=tcp:127.0.0.1:5760`

# Breadcrumb

- [GCS only operation](https://ardupilot.org/rover/docs/common-gcs-only-operation.html)
- [MAVSDK Manual control](http://mavsdk-python-docs.s3-website.eu-central-1.amazonaws.com/plugins/manual_control.html)
- [DroneKit Guided mode](https://dronekit-python.readthedocs.io/en/latest/examples/guided-set-speed-yaw-demo.html)
- [MAVROS setpoint_velocity](http://wiki.ros.org/mavros#mavros.2FPlugins.setpoint_velocity)
